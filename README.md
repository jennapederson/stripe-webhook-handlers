# Stripe Webhook Handlers

Small Sinatra app to handle webhook calls from Stripe.

# Running Locally

`STRIPE_KEY='sk_test_YOUR_KEY' MAILGUN_API_KEY='key-YOUR_KEY' STRIPE_ENDPOINT_SECRET='whsec_YOUR_SECRET' FROM_EMAIL_ADDRESS='YOUR_EMAIL' ruby email_receipts_webhook.rb`