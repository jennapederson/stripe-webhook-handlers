require 'sinatra'
require 'stripe'
require 'mailgun-ruby'
require 'sinatra/custom_logger'
require 'logger'

set :logger, Logger.new(STDOUT)

Stripe.api_key = ENV['STRIPE_KEY']
endpoint_secret = ENV['STRIPE_ENDPOINT_SECRET']
send_email = ENV['SEND_EMAIL'] == 'true'
from_email_address = ENV['FROM_EMAIL_ADDRESS']
mg_client = Mailgun::Client.new ENV['MAILGUN_API_KEY']

get '/ping' do
  "Pong!"
end

post '/webhook' do
  payload = request.body.read

  logger.info "PAYLOAD: #{payload}"

  # Verify signature to make sure the event came from Stripe
  signature = request.env['HTTP_STRIPE_SIGNATURE']
  event = nil

  begin
    event = Stripe::Webhook.construct_event(payload, signature, endpoint_secret)

    # Only respond to `charge.succeeded` events with a charge amount
    if event.type == 'charge.succeeded' && event.data.object.amount
      message_params = {
        from: from_email_address,
        to: from_email_address,
        subject: 'Your email receipt',
        text: text_email(event)
      }
      logger.info "Sending receipt email to: #{event.data.object.receipt_email}"
      logger.info message_params
      mg_client.send_message ENV['MAILGUN_DOMAIN'], message_params if send_email
    end

    status 200
  rescue JSON::ParserError => e
    # Invalid payload
    logger.info "ParserError #{e}"
    status 400
  rescue Stripe::SignatureVerificationError => e
    logger.info "SignatureVerificationError #{e}"
    status 400
  end
end

def format_amount(amount)
  # Format with approriate commas
  sprintf('$%0.2f', amount.to_f / 100.0).gsub(/(\d)(?=(\d{3})+(?!\d))/, "\\1,")
end

# Text email
def text_email(event)
  <<-EOF
  Hi #{event.data.object.billing_details.name} -

  Thank you so much for your support! Your #{format_amount(event.data.object.amount)} #{event.data.object.currency.upcase} donation is so helpful.

  Best,

  Jenna Pederson

  In accordance with IRS regulations, this serves as your gift receipt and confirms you received no goods or services in return for your contribution.

  =================================================================
  RECEIPT - Payment for #{event.data.object.description}


  Total: #{format_amount(event.data.object.amount)} #{event.data.object.currency}


  Charged to: #{event.data.object.payment_method_details.card.brand} ending in #{event.data.object.payment_method_details.card.last4}
  Payment ID: #{event.data.object.id}
  Date: #{Time.now.strftime("%m/%d/%Y")}


  =================================================================


  EOF
end